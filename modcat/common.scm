;;;; Common functions, not too specific to the module system. In
;;;; particular, we make no reference here to how the data tree looks
;;;; like.

(use file.util)
(use gauche.sequence)
(use srfi-13)
(use sxml.ssax)
(use sxml.sxpath)
(use sxml.tools)

;;; Generic accessors

;; We often use 'na as a default value for optional and key
;; parameters, so we know if it has been supplied. This is useful when
;; #f could be a legitimate value.
(define na (gensym))
(define (is-na? x) (eq? x 'na))

;; Some accessors return a list when in fact we only want one
;; item. This function get-unique extracts the only item in a one-item
;; list. The cases of an empty list or a list with more than one item
;; are handled properly, as is the case that #f is returned instead of
;; a list.
(define (get-unique lst :key (hint 'na) (default 'na))
  (cond
   ((not lst)
    (cond
     ((is-na? default) (error (format "get-unique: #f returned; hint: ~a" hint)))
     (else default)))
   (else
    (case (length lst)
      ((0) (cond
            ((is-na? default) (error (format "get-unique: empty list; hint: ~a" hint)))
            (else default)))
      ((1) (car lst))
      (else (error (format "get-unique: not unique: ~a; hint: ~a" lst hint)))))))

;; Wrapper around assoc-ref when we expect exactly one item.
(define (assoc-ref-1 alist key :key (default 'na))
  (get-unique (assoc-ref alist key) :hint (list alist key) :default default))

;; Wrapper around sxml:content when we expect exactly one item.
(define (sxml:content-1 obj :key (default 'na))
  (get-unique (sxml:content obj) :hint obj :default default))

;; Wrapper around sxml:attr that supports a default value and throws
;; an error when not found (and no default given).
(define (get-attr obj name :key (default 'na))
  (let ((x (sxml:attr obj name)))
    (cond
     (x x)
     ((is-na? default) (error (format "get-attr: ~a not found in: ~a" name obj)))
     (else default))))

;;; Generic list handling

;; Remove all sublist boundaries.
(define (flatten-lists lst)
  (cond
   ((null? lst) '())
   ((list? (car lst)) (flatten-lists (append (car lst) (cdr lst))))
   (else (cons (car lst) (flatten-lists (cdr lst))))))

;; Subdivide list LST into sublists of N items each.
(define (subdivide-list lst n)
  (let loop ((in lst) (out '()))
    (cond
     ((null? in) out)
     ((< (length in) n) (append out (list in)))
     (else (let-values (((a b) (split-at in n)))
             (loop b (append out (list a))))))))

;; Looks for OBJ in LST using equal? and returns its index if found.
(define (find-index-equal obj lst) (find-index (^x (equal? x obj)) lst))

;; Compare function to sort according to a list ORDER. Also ensures
;; that no other keys than those listed in ORDER occur.
(define (make-index-cmp order :key (key-fn (^x x)))
  (let ((fail (^k (error (format "index-cmp: illegal entry: ~s" k)))))
    (^(x y)
      (let ((x-pos (find-index-equal (key-fn x) order))
            (y-pos (find-index-equal (key-fn y) order)))
        (unless x-pos (fail x))
        (unless y-pos (fail y))
        (< x-pos y-pos)))))

;; Sort alist by keys, using a list ORDER to define order.
(define (sort-alist alist order)
  (sort alist (make-index-cmp order :key-fn car)))

;; Given a potentially nested structure of strings, collect them all
;; into one string, discarding the nesting structure. The starred
;; version takes this even further by discarding anything that is not
;; a string.
(define (smash-strings lst) ($ apply string-append $ flatten-lists lst))
(define (smash-strings* lst) ($ apply string-append $ filter string? $ flatten-lists lst))

;; Remove newlines and whitespace around newlines, keep only one space.
(define (one-line str) (regexp-replace-all #/s*\n\s*/ str " "))

;; Merge given alists, resolving conflicts based on
;; priorities. Priorities can be given for multiple entries at
;; once. For example:
;;
;; ((1 (alpha 1)
;;     (beta 2)
;;     (gamma 3))
;;  (2 (beta -2))
;;  (0 (delta 4)
;;     (gamma -1)))
;;
;; This yields, in different order perhaps:
;;
;; ((alpha 1) (beta -2) (gamma 3) (delta 4))
;;
(define (prio-merge alist-with-prios)
  (let ((taken-at-prio (make-hash-table 'eq?))
        (res-ht (make-hash-table 'eq?)))
    (for-each
     (^l (let ((prio (car l)))
           (for-each (^f
                      (let ((take-this (^() (hash-table-put! taken-at-prio (car f) prio)
                                         (hash-table-put! res-ht (car f) (cdr f)))))
                        (cond
                         ((hash-table-get taken-at-prio (car f) #f) => (^p (when (< p prio) (take-this))))
                         (else (take-this)))))
                     (cdr l))))
     alist-with-prios)
    (hash-table->alist res-ht)))

;; Generalization of map that is particularly useful for trees given
;; as nested lists. Basic idea: call BRANCH-FUN on branches (and then
;; descent), and call LEAF-FUN on leafs. With :CONTENT? #t, provides
;; special treatment of SXML structures, where BRANCH-FUN and descent
;; is only applied to the content.
(define (map-tree branch-fun tree :key (leaf-fun (^x x)) (empty? #f) (content? #f))
  (let ((map% (^l (map (^x (map-tree branch-fun x :leaf-fun leaf-fun :empty? empty? :content? content?))
                       (branch-fun l)))))
    (cond
     ((and (not empty?) (list? tree) (null? tree)) '())
     ((and content? (sxml:element? tree)) (call-with-content map% tree))
     ((list? tree) (map% tree))
     (else (leaf-fun tree)))))

;;; Hash tables

;; For hash tables where the values are lists. Insert VAL at the end
;; of the list stored under KEY.
(define (hash-table-list-push! ht key val)
  (cond
   ((hash-table-get ht key #f) => (^l (hash-table-put! ht key `(,@l ,val))) )
   (else (hash-table-put! ht key `(,val)))))

;;; SXML handling

;; Perhaps could also use as-nodeset instead.
(define (ensure-list x) (if (list? x) x (list x)))

;; Isolate name and attribute list. Call FUN on content. Return name,
;; attribute list, and whatever FUN yielded.
(define (call-with-content fun obj)
  (let ((attr (sxml:attr-list-node obj))
        (fun-res (ensure-list (fun (sxml:content obj)))))
    (cond
     (attr
      `(,(sxml:element-name obj)
        ,attr
        ,@fun-res))
     (else
      `(,(sxml:element-name obj)
        ,@fun-res)))))

;; Walk tree, and if an element's content begins with a string, then
;; left-trim this string, and if it ends the a string, then right-trim
;; it. This should get rid of most of the unwanted whitespace.
(define (call-on-first-string% fun lst)
  (cond
   ((and (pair? lst) (string? (car lst))) (cons (fun (car lst)) (cdr lst)))
   (else lst)))
(define (trim-first-last-string% lst)
  (call-on-first-string% string-trim (reverse (call-on-first-string% string-trim-right (reverse lst)))))
(define (trim-whitespace tree)
  (map-tree trim-first-last-string% tree :content? #t ))

;; Basic XML slurping.
(define (slurp-xml fn)
  (call-with-input-file fn (^p (ssax:xml->sxml p '()))))

;;; Files and directories

;; Make sure that dir is an empty directory.
(define (make-fresh-directory dir)
  (cond
   ((file-is-directory? dir) (remove-directory* dir))
   ((file-exists? dir) (error "make-fresh-directory: something in the way: ~a" dir)))
  (make-directory* dir))

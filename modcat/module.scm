;;;; Module system


;;; The <data> class

;; Class to store the complete input, which is provided in the form of
;; XML files. There is only one method, namely a constructor.

(define-class <data> ()
  ((tree :accessor tree-of :immutable #t)))

;; Slurp XML files, merge data, remove whitespace.
(define-method initialize ((data <data>) fn-list)
  (next-method)
  (set! (tree-of data)
    (trim-whitespace
     `(*TOP*
       (data
        ,@(apply
           append
           (map
            (^(fn) (cdar ((sxpath '(data)) (slurp-xml fn))))
            fn-list))))))
  data)


;;; The *DATA* parameter and its accessors

;; There will be only one <data> instance around, namely the one in
;; the parameter *DATA*. We only access it via by-id and by-type.

(define *data* (make-parameter #f))
(define (by-id type id :key (default 'na))
  (get-unique
   ((sxpath `(data (,type (@ id (equal? ,(x->string id)))))) (tree-of (*data*)))
   :hint (list type id) :default default))
(define (by-type type)
  ((sxpath `(data (,type))) (tree-of (*data*))))


;;; Simple extractors and filters

(define (get-id obj) (get-attr obj 'id))
(define (ids-of lst) (map get-id lst))
(define (not-flag? obj flag) (not (get-attr obj flag :default #f)))
(define (not-hidden? obj) (not-flag? obj 'hidden))
(define (not-draft? obj) (not-flag? obj 'draft))

;;; Content/ID --> Content transformations

;; Expansions that are used in multiple places. Given a tree CONTENT,
;; recursively handle its <lang>, <text>, <param> elements; also
;; handle <label> and <content>, which are used in templates. For
;; <content>, the optional parameter EXPANDED-MODULE must be
;; specified.
;;
;; This is the only function using the *LANG* parameter.
;;
;; The result might not be a single string yet, but a nested
;; structure. Use smash-strings to get a single string.
(define *lang* (make-parameter #f))
(define (expand-common content :optional (expanded-module 'na))
  (let loop ((content content) (params '()))
    (let ((fail (^x (error (format "expand-common: ~a" x)))))
      (map (^y (cond
                ((string? y) y)
                ((sxml:element? y)
                 (case (sxml:element-name y)
                   ((lang language)     ; accept both for convenience
                    (cond
                     ((string=? (get-attr y 'name) (*lang*)) (loop (sxml:content y) params))
                     (else '())))
                   ((text label)
                    => (^x (let ((id (sxml:content-1 y)))
                             (loop (sxml:content (by-id x id)) (sxml:attr-list y)))))
                   ((param)
                    ($ assoc-ref-1 params $ string->symbol $ sxml:content-1 y))
                   ((content)
                    (assoc-ref-1 expanded-module (sxml:content-1 y)))
                   (else (fail y))))
                (else (fail content))))
           content))))

;; (ID --> Content) Convenience function based on expand-common.
(define (expand-common-by-id type id)
  (smash-strings (expand-common (sxml:content (by-id type id)))))

;; Module categories grouped by study programs and sorted according to
;; program and category definitions given in XML. We expect CONTENT to
;; be empty or to contain exactly one string, which will be split.
(define (structure-struct content)
  (cond
   ((null? content) '())
   (else
    (let* ((res (map
                 (^s (cons (caar s) (map cadr s)))
                 (group-collection
                  (subdivide-list
                   (map string-trim-both (remove string-null? (string-split (car content) " ")))
                   2)
                  :key car :test string=?)))
           (res (sort-alist res ($ ids-of $ by-type 'program)))
           (res (map (^x (cons (car x) (sort (cdr x) (make-index-cmp ($ ids-of $ by-type 'category))))) res)))
      res))))

;; Supposed to work on the output of structure-struct.
(define (expand-struct content)
  (let* ((res (map (^x (cons (expand-common-by-id 'program (car x)) (cdr x))) content))
         (res (map (^x (cons (car x) (map (^y (expand-common-by-id 'category y)) (cdr x)))) res)))
    res))


;;; Module/ID --> Module transformations

;; 1st step: recursively expand all the <mixin> directives.
(define (module-expand-mixins module)
  (call-with-content
   (^c (prio-merge
        (map
         (^f
          (case (sxml:element-name f)
            ((mixin)
             (let ((prio (x->number (or (sxml:attr f 'prio) 0))))
               (cond ((by-id 'module (sxml:content-1 f) :default #f)
                      => (^m (cons prio (cdr (filter (sxml:invert (ntype?? '@))
                                                     (module-expand-mixins m))))))
                     (else (error (format "module-expand-mixins: cannot find: ~a" (sxml:content-1 f)))))))
            (else (list 0 f))))
         c)))
   module))

;; 2nd step: sort fields according to the list of label IDs plus
;; 'struct, also throwing an error if there is a field not mentioned
;; in this list.
(define (module-sort-fields module)
  (call-with-content
   (^c (sort-alist c `(,@($ map string->symbol $ ids-of $ by-type 'label) struct)))
   module))

;; 3rd step: expand all fields with either expand-common or
;; structure-struct.
(define (module-expand-fields module)
  (call-with-content
   (^c
    (map (^f
          (case (car f)
            ((struct) (call-with-content ($ expand-struct $ structure-struct $) f))
            (else (call-with-content ($ smash-strings $ expand-common $) f))))
         c))
   module))

;; Chain the three above functions.
(define (fully-expand-module module)
  ($ module-expand-fields $ module-sort-fields $ module-expand-mixins module))

;; (ID --> Module) Convenience function based on fully-expand-module.
(define (fully-expand-module-by-id id)
  ($ fully-expand-module $ by-id 'module id))


;;;; Template expansion

;; List template expansion. Relevant elements are <list> and
;; <item>. TREE must provide proper structure.
(define (template-expand-list template tree :optional (item #f))
  (call-with-content
   (^(content)
     (map (^y (cond
               ((and (sxml:element? y) (eq? (sxml:element-name y) 'item))
                (unless item (error (format #f "template-expand-list: item outside of list: ~a" template)))
                item)
               ((and (sxml:element? y) (eq? (sxml:element-name y) 'list))
                (unless tree (error (format #f "template-expand-list: out of list content: ~a" template)))
                (call-with-content
                 (^c
                  (intersperse
                   "\n"
                   (map
                    (^(branch/leaf)
                      (cond
                       ((list? branch/leaf) (template-expand-list c (cdr branch/leaf) (car branch/leaf)))
                       (else (template-expand-list c #f branch/leaf))))
                    tree)))
                 y))
               (else y)))
          content))
   template))


;;;; Global processing

;; Get structured (but non-expanded) struct of module.
(define (get-struct module)
  (structure-struct (sxml:content (assoc-ref (sxml:content module) 'struct))))

;; Create a hash table with keys of the form (study-program
;; module-category) and the corresponding lists of module IDs as
;; values.
(define (reverse-structure)
  (let ((res (make-hash-table 'equal?)))
    (for-each
     (^m
      (for-each
       (^(prog/cats)
         (let ((prog (car prog/cats))
               (cats (cdr prog/cats)))
           (for-each
            (^c (hash-table-list-push! res `(,prog ,c) (get-attr m 'id)))
            cats)))
       ($ get-struct $ module-expand-mixins m)))
     ($ filter not-hidden? $ filter not-draft? $ by-type 'module))
    res))

;; Write representations of all modules into directory 'all' under
;; BASE. For now, we use fully-expand-module to create those
;; representations; later, we will use a function that expands a
;; template.
(define (write-all-modules base)
  (let ((dir (build-path base "all")))
    (make-fresh-directory dir)
    (for-each
     (^m
      (call-with-output-file (build-path dir (format #f "~a.tex" (get-id m)))
        (^p (write (fully-expand-module m) p))))
     ($ filter not-draft? $ by-type 'module))))

;; Create directory structure with symlinks representing the structure
;; of our study courses and module categories. Link targets are the
;; files under 'all', created by write-all-modules.
(define (link-all-modules base)
  (make-fresh-directory (build-path base "struct"))
  (hash-table-for-each
   (reverse-structure)
   (^(prog/cat id-list)
     (let ((dir (build-path base "struct" (car prog/cat) (cadr prog/cat))))
       (make-directory* dir)
       (for-each
        (^(id)
          (let ((fn (format #f "~a.tex" id)))
            (sys-symlink (build-path "../../../all" fn) (build-path dir fn))))
        id-list)))))

#!/usr/bin/env gosh
;; -*- scheme -*-
(include "common.scm")
(include "module.scm")

(define (test-expand-module fn-list id lang expected-content)
  (parameterize ((*data* (apply make `(,<data> ,@fn-list)))
                 (*lang* lang))
    (let ((content (sxml:content (fully-expand-module-by-id id))))
      (assert (equal? content expected-content)))))

(define (test-expand-module-series fn-list . lst)
  (for-each (^x (apply test-expand-module (cons fn-list x))) lst))

(define (test-1)
  (test-expand-module-series
   '("infra.xml" "test1.xml")
   '("mod01" "de" ((Title "Module 01") (Summary "Dies ist Modul Nummer 1.")))
   '("mod01" "en" ((Title "Module 01") (Summary "This is module number 1.")))
   '("mod02" "en" ((Title "Module No. 02") (Code "mathMod02")))
   '("mod02" "de" ((Title "Modul Nr. 02") (Code "mathMod02")))
   '("mod03" "de" ((Title "Module 01") (Summary "Dies ist Modul Nummer 1.")))
   '("mod03" "en" ((Title "Module 01") (Summary "This is module number 1.")))
   '("mod04" "en" ((Title "Module No. 02") (Code "mathMod03")))
   '("mod04" "de" ((Title "Modul Nr. 02") (Code "mathMod03")))
   '("mod05" "en" ((Title "Module No. 02") (Code "mathMod02")))
   '("mod05" "de" ((Title "Modul Nr. 02") (Code "mathMod02")))
   '("mod06" "en" ((Title "Module No. 02") (Code "mathMod06")
                   (struct ("Bachelor, 1-Fach, Mathematik (Version 2007/17)"
                            "Elective Area Pure Mathematics"))))
   '("mod06" "de" ((Title "Modul Nr. 02") (Code "mathMod06")
                   (struct ("Bachelor, 1-Fach, Mathematik (Version 2007/17)"
                            "Wahlbereich Reine Mathematik"))))
   '("mod07" "de" ((Title "Modul Nr. 02")
                   (Code "mathMod02")
                   (ContactWorkload "Dieses Modul hat 123 Stunden Vorlesung.")
                   (Summary "Dies ist Modul Nummer 1.")
                   (Content "Dieses Modul hat viele Dinge: gamma, beta, alpha.")
                   (struct ("Bachelor, 1-Fach, Mathematik (Version 2007/17)"
                            "Pflicht" "Wahlbereich Reine Mathematik")
                     ("Master, 1-Fach, Finanzmathematik (Version 2007/17\\added{/23})"
                      "Wahlbereich Reine Mathematik"))))
   ))

(define (test-expand-template-list fn-list id tree expected-content)
  (parameterize ((*data* (apply make `(,<data> ,@fn-list))))
    (let ((str (one-line
                (smash-strings*
                 (sxml:content
                  (template-expand-list (by-id 'template id) tree))))))
      (assert (equal? str expected-content)))))

(define (test-2)
  (test-expand-template-list
   '("infra.xml")
   "plan"
   '(("foo" "foo.1" "foo.2") ("bar" "bar.1" "bar.2" "bar.3"))
   "\\begin{description} \\item[foo:] \\begin{itemize}[leftmargin=0em] \\item foo.1 \\item foo.2 \\end{itemize} \\item[bar:] \\begin{itemize}[leftmargin=0em] \\item bar.1 \\item bar.2 \\item bar.3 \\end{itemize} \\end{description}"))

(test-1)
(test-2)
(print "All tests passed.")
